﻿using UnityEngine;
using System.Collections;

public class GravityWell : MonoBehaviour
{

    public Camera mainCamera;
    Rigidbody2D myRigidBody;
    float pullForce = 20;
    float lifetime = 1;
    float curLife = 0;

    // Use this for initialization
    void Start ()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        mainCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        curLife += Time.deltaTime;
        if (curLife > lifetime)
        {
            Destroy(gameObject);
        }

        transform.Rotate(-Vector3.forward);

	}

    //Pull the ball in
    void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Ball")
        {
            Vector3 dashVector = transform.position - coll.transform.position;

            dashVector.Normalize();
            dashVector *= pullForce;
            coll.attachedRigidbody.AddForce(dashVector);                                   
        }
    }
}
