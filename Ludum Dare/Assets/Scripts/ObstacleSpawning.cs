﻿using UnityEngine;
using System.Collections;

public class ObstacleSpawning : MonoBehaviour {

    public GameObject SpikePrefab;
    public GameObject BlockPrefab;

    public GameObject SpawnPosition;
    public GameObject BlockFolder; // Parent object in scene to put obstacles in

    public float SpawnTimer;
    [Range(0,100)]
    public float SpikeProbability;
    public int SpikeCoolDown;

    public float offset;
    private GameObject lastSpawned;
    private int numspikes;
    private int cooldown;

    void Start()
    {
        lastSpawned = SpawnPosition;
        numspikes = 0;
        cooldown = 0;
    }


    void OnTriggerExit2D()
    {
        if (numspikes == 0 && cooldown <= 0)
        {
            numspikes = RollForSpikes();
            if(numspikes > 0)
            {
                cooldown = SpikeCoolDown;
            }
        }
        if (numspikes > 0 && cooldown > 0)
        {
            lastSpawned = GameObject.Instantiate(SpikePrefab, new Vector3(lastSpawned.transform.position.x+offset, lastSpawned.transform.position.y, lastSpawned.transform.position.z), Quaternion.identity) as GameObject;
            lastSpawned.transform.SetParent(BlockFolder.transform);
            numspikes--;
            cooldown--;
        }
        else
        {
            lastSpawned = GameObject.Instantiate(BlockPrefab, new Vector3(lastSpawned.transform.position.x + offset, lastSpawned.transform.position.y, lastSpawned.transform.position.z), Quaternion.identity) as GameObject; cooldown--;
            lastSpawned.transform.SetParent(BlockFolder.transform);
        }
    }

    public int RollForSpikes()
    {
        int numspikes = 0;

        if(Random.Range(0f,100f) > SpikeProbability)
        {
            numspikes = Random.Range(1, 4);
        }

        return numspikes;
    }
}
