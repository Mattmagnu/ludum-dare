﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public Vector2 mBounceForce;
    public float jumpForce = 750;
    public float drawConstant = 0.25f;
    public float dashRegenConstant = 0.75f;

    public Camera mainCamera;
    public GameObject myWallPrefab;
    public GameObject myGravityPrefab;

    public AudioSource airDashSource;
    public AudioSource quackSource;

    bool isDrawing = false;
    bool canDash = true;
    float drawTime = 0;
    float dashRegenTime = 0;
    Rigidbody2D myRigidBody;
    GameObject myWell = null;

    public TrailRenderer myTrailRenderer;
    public float killTrailTime = 2;
    public float curTrailTime = 0;

    private bool facingRight = true;
	// Use this for initialization
	void Start ()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        //if(myRigidBody.velocity.magnitude <= killTrailSpeed)
        //{
        //    myTrailRenderer.enabled = false;
        //}
        //Spawn lines of walls behind us
        if (isDrawing)
        {
            GameObject newCube = GameObject.Instantiate(myWallPrefab);
            newCube.transform.position = transform.position;

            if (drawTime > drawConstant)
            {
                isDrawing = false;
                drawTime = 0;
            }
        }

        //Kill trail renderer if we're passed time
        if (curTrailTime > killTrailTime)
        {
            myTrailRenderer.enabled = false;
        }

        //Jump towards the mouse position
        if (canDash && Input.GetKeyDown(KeyCode.Mouse0))
        {
            isDrawing = false;
            drawTime = 0;

            AirDaish();
            //Dash Animation
            gameObject.GetComponent<Animator>().SetTrigger("MouseClick");

            //Turn on trail renderer
            myTrailRenderer.enabled = true;
            curTrailTime = 0;

            isDrawing = true;
            airDashSource.Play();
        }

        //Quack
        if (!myWell && Input.GetKeyDown(KeyCode.Mouse1))
        {
            quackSource.Play();

            //Actually gravity wells
            myWell = GameObject.Instantiate(myGravityPrefab);
            myWell.transform.position = transform.position;
        }

        if (dashRegenTime > dashRegenConstant)
        {
            canDash = true;
            dashRegenTime = 0;
        }

        drawTime += Time.deltaTime;
        dashRegenTime += Time.deltaTime;
        curTrailTime += Time.deltaTime;
        BoundingBox();

    }

    //Used to dash towards the mouse
    void AirDaish()
    {
        myRigidBody.velocity = Vector3.zero;
        Vector3 dashVector = Input.mousePosition - mainCamera.WorldToScreenPoint(transform.position);
        dashVector.Normalize();
        dashVector *= jumpForce;
        myRigidBody.AddForce(dashVector);
        canDash = false;

        if (dashVector.x < 0 && facingRight)
        {
            transform.Rotate(Vector3.up, 180f);
            facingRight = false;
        }
        else if (dashVector.x > 0 && !facingRight)
        {
            transform.Rotate(Vector3.up, 180f);
            facingRight = true;
        }
    }

    void BoundingBox()
    {
        Vector3 startPosition =transform.position;
        Rect cameraPosition = mainCamera.rect;
        Vector3 spriteExtents = mainCamera.WorldToViewportPoint(GetComponent<SpriteRenderer>().bounds.extents);
        float left = -8.2f;
        float right = 8.5f;
        float top = 4.5f;
        float bottom = -3.9f;

        if (left > startPosition.x)
        {
            transform.position = new Vector3(left, startPosition.y, startPosition.z);
        }
        if (right < startPosition.x)
        {
            transform.position = new Vector3(right, startPosition.y, startPosition.z);
        }
        if(top < startPosition.y)
        {
            transform.position = new Vector3(startPosition.x, top, startPosition.z);
        }
        if(bottom > startPosition.y)
        {
            transform.position = new Vector3(startPosition.x, bottom, startPosition.z);
        }
        //if (cameraPosition.xMax > startPosition.x)
        //{
        //    Debug.Log("2");
        //    Debug.Log("xmin" + cameraPosition.xMax);
        //    Debug.Log("startpos" + (startPosition.x - spriteExtents.x));
        //    transform.position = mainCamera.ViewportToWorldPoint(new Vector3(cameraPosition.xMax, startPosition.y, startPosition.z));
        //}
        //if (cameraPosition.yMin < startPosition.y - spriteExtents.y)
        //{
        //    Debug.Log("3");
        //    transform.position = mainCamera.ViewportToWorldPoint(new Vector3(startPosition.x, cameraPosition.yMin + spriteExtents.y, startPosition.z));
        //}
        //if (cameraPosition.xMax > startPosition.x + spriteExtents.y)
        //{
        //    Debug.Log("3");
        //    transform.position = mainCamera.ViewportToWorldPoint(new Vector3(startPosition.x, cameraPosition.yMax - spriteExtents.y, startPosition.z));
        //}

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
       // Vector2 forceToAdd = new Vector2(0, 0);


        if (coll.transform.name == "PfSpike(Clone)")
        {
            Die();
        }

        //if (coll.transform.name == "PfBlock(Clone)" || coll.transform.name == "PfBlock")
        //{
        //    forceToAdd.y = mBounceForce.y;
        //}

        //if (coll.transform.name == "Top")
        //{
        //    forceToAdd.y = -mBounceForce.y;

        //}

        //if (coll.transform.name == "Left")
        //{
        //    forceToAdd.x = mBounceForce.x;
        //}

        //if (coll.transform.name == "Right")
        //{
        //    forceToAdd.x = -mBounceForce.x;
        //}

        //if (forceToAdd.magnitude > 0)
        //{
        //    myRigidBody.velocity = new Vector3(0.0f, 0.0f, 0.0f);
        //}
        ////Apply the force
        //myRigidBody.AddForce(forceToAdd);
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
