﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class Score : MonoBehaviour
{
    public int score;
    public Text scoreText;
    public GameObject highScoreList;
    public GameObject nameInput;
    public GameObject button;
    List<string> highScoreNames = new List<string>();
    List<int> highScores = new List<int>();

    bool stillPlaying = true;
    bool readyToGo = false;
    

	// Use this for initialization
	void Start ()
    {
        score = 0;
        ReadHighScores();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (stillPlaying)
        {
            ++score;
            scoreText.text = "Score: " + score;
        }

        if (readyToGo && Input.anyKey)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Main Menu");
        }
	}

    //Do the things the submit button wants to do
    public void SubmitButton()
    {
        if (nameInput.GetComponent<InputField>().text.Length > 0)
        {
            RecordNewHighScore();
            DisplayHighScores();
        }
    }

    //Used to store high score if it is better than any of the current scores
    void RecordNewHighScore()
    {
        for (int i = 0; i < 5; ++i)
        {
            if (score > highScores[i])
            {
                highScores.Insert(i, score);
                highScoreNames.Insert(i, nameInput.GetComponent<InputField>().text);
                highScoreNames[i] = highScoreNames[i].Replace(' ', '_');
                highScores.RemoveAt(5);
                highScoreNames.RemoveAt(5);

                WriteHighScores();
                break;
            }
        }
    }

    //Read high scores from a file. Create a default high score file if one does not yet exist
    void ReadHighScores()
    {
        string path = Application.persistentDataPath + "/highScore.txt";
        if (!File.Exists(path))
        {
            InitHighScores(path);
        }
        
        StreamReader reader = new StreamReader(path);
        for (int i = 0; i < 5; ++i)
        {
            string[] nameAndScore = reader.ReadLine().Split(' ');
            highScoreNames.Add(nameAndScore[0]);
            highScores.Add( int.Parse(nameAndScore[1]) );
        }

        reader.Close();
    }

    //Write our current high scores to the file
    void WriteHighScores()
    {
        string path = Application.persistentDataPath + "/highScore.txt";
        StreamWriter writer = new StreamWriter(path, false);
        for (int i = 0; i < 5; ++i)
        {
            writer.Write(highScoreNames[i] + ' ' + highScores[i].ToString() + '\n');
        }
        writer.Close();
    }

    //Initialize default high score file
    void InitHighScores(string path)
    {
        StreamWriter writer = new StreamWriter(path);
        writer.WriteLine("KM 100\nKM 100\nKM 100\nKM 100\nKM 100");
        writer.Close();
    }

    //Display top 5 high score list
    void DisplayHighScores()
    {
        scoreText.text = "";
        nameInput.SetActive(false);
        button.SetActive(false);
        highScoreList.SetActive(true);
        Text highScoresText = highScoreList.GetComponent<Text>();
        highScoresText.text = "--High Scores--";
        
        for (int i = 0; i < 5; ++i)
        {
            highScoresText.text += '\n' + highScoreNames[i] + ' ' + highScores[i].ToString() + '\n';
        }

        readyToGo = true;
    }

    //Display name input prompt
    public void PromptForName()
    {
        if (stillPlaying)
        {
            stillPlaying = false;
            nameInput.SetActive(true);
            button.SetActive(true);
            nameInput.GetComponent<InputField>().ActivateInputField();
        }
    }
}
