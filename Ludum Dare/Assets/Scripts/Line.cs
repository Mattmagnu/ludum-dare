﻿using UnityEngine;
using System.Collections;

public class Line : MonoBehaviour {

    public float deathTimer = 0.5f;
    public float curTimer = 0;
    public ParticleSystem myParticleSystem;

	// Use this for initialization
	void Start ()
    {
        myParticleSystem.startColor = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
    }
	
	// Update is called once per frame
	void Update ()
    {
        curTimer += Time.deltaTime;
        if (curTimer > deathTimer)
        {
            Destroy(gameObject);
        }
	}
}
