﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{

    public Vector2 mBounceForce;
    public Vector2 mGrowthFactor;
    public Score mScore;
    private Rigidbody2D mRigidBody;
    private AudioSource mAudioSource;
    

    // Use this for initialization
    void Start()
    {
        mRigidBody = transform.GetComponent<Rigidbody2D>();
        mAudioSource = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update ()
    {
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.transform.name == "PfSpike(Clone)")
        {
            Die();
        }


        Vector2 forceToAdd = new Vector2();
        float fudgeFactor = 0.2f;

        //Print outour position and the collision point
        //Debug.Log("Collision occured!");
        //Debug.Log("Collision point: " + coll.contacts[0].point);
        //Debug.Log("Our Position: " + transform.position);


        //If the collision distance is significant enough
        if (Mathf.Abs(Mathf.Abs(transform.position.y) - Mathf.Abs(coll.contacts[0].point.y)) > fudgeFactor)
        {
            //Find the y force
            if (coll.contacts[0].point.y < transform.position.y)
            {
                forceToAdd.y = mBounceForce.y;
            }
            else if (coll.contacts[0].point.y > transform.position.y)
            {
                forceToAdd.y = -mBounceForce.y;

            }
        }

        //If the collision distance is significant enough
        if (Mathf.Abs(Mathf.Abs(transform.position.x) - Mathf.Abs(coll.contacts[0].point.x)) > fudgeFactor)
        {
            //Find the x force
            if (coll.contacts[0].point.x < transform.position.x)
            {
                forceToAdd.x = mBounceForce.x;
            }
            else if (coll.contacts[0].point.x > transform.position.x)
            {

                forceToAdd.x = -mBounceForce.x;
            }
        }

        //Apply the force
        mRigidBody.AddForce(forceToAdd);
        Grow();
        PlaySound();
    }

    //Make the ball bigger
    void Grow()
    {
        transform.localScale += new Vector3(mGrowthFactor.x, mGrowthFactor.y, 0);
        mBounceForce.x += 10;
        mBounceForce.y += 10;

    }

    //Play our sound
    void PlaySound()
    {
        mAudioSource.Play();
    }

    void Die()
    {
        mScore.PromptForName();
        Destroy(gameObject);
    }
}
