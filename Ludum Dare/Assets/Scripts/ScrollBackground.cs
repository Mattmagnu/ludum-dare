﻿using UnityEngine;
using System.Collections;

public class ScrollBackground : MonoBehaviour {

    public GameObject Backgroundmover;

    public float scrollspeed;

    private Vector3 initialPos;
	// Use this for initialization
	void Start () {
        initialPos = Backgroundmover.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        Backgroundmover.transform.Translate(new Vector3(-scrollspeed * Time.deltaTime, 0, 0));
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.transform.name == "ResetTrigger")
        {
            Backgroundmover.transform.position = initialPos;
        }
    }
}
