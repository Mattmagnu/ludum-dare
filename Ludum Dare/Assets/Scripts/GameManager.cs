﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public enum GameState {
		Start,
		Game,
		GameOver,
		NoState,
	}
	
	private  GameState currentState = GameState.NoState;
	
	public void OnPlay(){
		Debug.Log("Pressed Play");
		SetState(GameState.Game);
	}
	
	public void SetState(GameState _newState){
		if(currentState != _newState){
			switch(_newState){
				case GameState.Start:
					UnityEngine.SceneManagement.SceneManager.LoadScene("Main Menu");
				break;
				case GameState.Game:
					UnityEngine.SceneManagement.SceneManager.LoadScene("VideoGame");
				break;
				case GameState.GameOver:
				break;
				default:
				break;
			}
		}
	}
}
