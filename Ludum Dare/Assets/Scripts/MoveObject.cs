﻿using UnityEngine;
using System.Collections;

public class MoveObject : MonoBehaviour {

    public float speed;
    public Direction myDirection;

    public enum Direction
    {
        Left,
        Right,
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(myDirection == Direction.Left)
        {
            transform.Translate(new Vector3(-speed*Time.deltaTime, 0));
        }
        else if (myDirection == Direction.Right)
        {
            transform.Translate(new Vector3(speed * Time.deltaTime, 0));
        }
	}
}
