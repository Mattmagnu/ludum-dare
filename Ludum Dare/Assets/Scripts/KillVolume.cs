﻿using UnityEngine;
using System.Collections;

public class KillVolume : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.name != "ResetTrigger")
        {
            GameObject.Destroy(other.gameObject);
        }
    }
}
